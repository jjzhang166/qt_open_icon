﻿/**
 ** @author:	   王世雄
 ** @date:        2019.3.20
 ** @brief:        进度状态条
 ** 1.strList 为进度状态条中的字符串列表
 ** 2.unfinishImg/finishImg 分别为状态改变前后的图片，线条会根据图片（10，10）位置的像素自动设置颜色
 ** 3.具有自适应功能
 */
#include "progressstate.h"

ProgressState::ProgressState(QVector<QString> strList,QString finishImg, QString unfinishImg, QWidget *parent)
    : QWidget(parent)
{
    for(int i = 0; i < strList.size(); i++)
    {
        textRectList << fontMetrics().boundingRect(strList.at(i));
    }
    this->strList = strList;
//    setAttribute(Qt::WA_TranslucentBackground, true);
    initMembers();
    setMargin(0);
    setIndexCount(5);
    setCurrentIndex(0);
    QImage *unImg = new QImage;
    QImage *img = new QImage;
    unImg->load(unfinishImg);
    unfinishedImg = new QImage(unImg->scaled(width,width,Qt::KeepAspectRatio,Qt::SmoothTransformation));
    img->load(finishImg);
    finishedImg = new QImage(img->scaled(width,width,Qt::KeepAspectRatio,Qt::SmoothTransformation));
    finishColor = QColor(finishedImg->pixel(10,10));
    unfinishColor = QColor(unfinishedImg->pixel(10,10));
    delete img;
    delete unImg;
}

ProgressState::~ProgressState()
{

}

void ProgressState::setMargin(int margin)
{
    if(this->margin != margin) {
        this->margin = margin;
        setMinimumHeight(this->margin*2+12+24);
        refreshContectsRect();
    }
}

int ProgressState::getIndexCount()
{
    return pointList.size();
}

void ProgressState::setIndexCount(int indexCount)
{
    if(this->indexCount != indexCount) {
        this->indexCount = indexCount;
        refreshPoints();
    }
}

void ProgressState::setCurrentIndex(int index)
{
    if(this->index != index) {
        this->index = index;
    }
}

int ProgressState::currentIndex() const
{
    return index;
}

void ProgressState::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter *painter= new QPainter(this);
    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::NoBrush);
    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    // 绘制图形项
    drawGroove(painter);

    delete painter;
    painter = 0;
}

void ProgressState::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    refreshContectsRect();
    refreshPoints();
}

void ProgressState::initMembers()
{
    indexCount = 0;
    index = 0;
    width = 30;
    height = 4;
}

void ProgressState::drawGroove(QPainter *painter)
{
//    painter->setFont(QFont(a2w("微软雅黑"), 12));
    for(int i = 0; i < indexCount; ++i)
    {
        painter->setPen(QPen(QBrush(QColor(0, 0, 0)), 2));
        QRectF rctText(pointList.at(i) + QPointF(-textRectList.at(i).width()/2, -textRectList.at(i).height()/2 - width), QSize(textRectList.at(i).width(), textRectList.at(i).height()));
        painter->drawText(rctText, Qt::AlignCenter, strList.at(i));
        if(i >= index)
        {
            painter->setPen(QPen(QBrush(unfinishColor), 4));
            painter->drawImage(pointList.at(i).x()-width/2.0,pointList.at(i).y()-width/2.0,*unfinishedImg);
        }
        else
        {
            painter->setPen(QPen(QBrush(finishColor), 4));
            painter->drawImage(pointList.at(i).x()-width/2.0,pointList.at(i).y()-width/2.0,*finishedImg);
        }
        if(i + 1 < indexCount)
            painter->drawLine(pointList.at(i)+QPointF(width/2.0,0),pointList.at(i+1)-QPointF(width/2.0, 0));

    }
}

void ProgressState::refreshContectsRect()
{
    rctContents = rect().adjusted(margin, margin, -margin, -margin);
    refreshPoints();
}

void ProgressState::refreshPoints()
{
    float vMargin = 0.0;
    if(textRectList.first().width() > textRectList.last().width())
        vMargin = textRectList.first().width() / 2.0;
    else
        vMargin = textRectList.last().width() / 2.0;

    float offset = (rctContents.width()-width*indexCount - vMargin*2)/double(indexCount-1);
    float handlerY = rctContents.center().ry();

    pointList.clear();
    for(int i = 0; i < indexCount; ++i) {
        pointList.append(QPoint(rctContents.left()+offset*i+width*(i*2+1)/2.0+vMargin, handlerY));
    }
}
