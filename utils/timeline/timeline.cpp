﻿#include "timeline.h"
#include <QDebug>

TimeLine::TimeLine(QWidget *parent) : QWidget(parent)
{
    dateTimeStart = QDateTime::currentDateTime();
    pressDown = false;
    isInside = false;
    setMouseTracking(true);
}

void TimeLine::updateTime(QDateTime dateTime)
{
    if(pressDown)
        return;
    dateTimeStart = dateTime;
    update();
}

void TimeLine::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    drawBackground(&painter);
    drawGroove(&painter);
    drawHandle(&painter);
    drawTicks(&painter);
}

void TimeLine::mousePressEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
    pressPoint = e->pos();
    pressDown = true;
}

void TimeLine::mouseMoveEvent(QMouseEvent *e)
{
//    Q_UNUSED(e);
    isInside = true;
    mousePoint = e->pos();
    if(pressDown)
    {
        offset = QPoint(e->x() - pressPoint.x(), e->y() - pressPoint.y());
//        update();
//        emit dateTimeSig(dateTimeStart.addSecs(-1 * 12.0 * offset.x() / width() * 3600));
    }

    update();
}

void TimeLine::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
    emit dateTimeSig(dateTimeStart.addSecs(-1 * 12.0 * offset.x() / width() * 3600));
    dateTimeStart = dateTimeStart.addSecs(-1 * 12.0 * offset.x() / width() * 3600);
    offset = QPoint(0, 0);
    pressDown = false;
}

void TimeLine::leaveEvent(QEvent *e)
{
    Q_UNUSED(e);
    isInside = false;
    update();
}

void TimeLine::drawBackground(QPainter *painter)
{
    painter->save();
    painter->fillRect(rect(), QColor(0, 0, 0));
    painter->restore();
}

void TimeLine::drawGroove(QPainter *painter)
{
    painter->save();
    QPen pen(QColor(75, 75, 75));
    painter->setPen(pen);
    pen.setWidth(3);
    // 绘制沟道背景
    painter->drawLine(QPointF(0, 3 * height() / 4),QPointF(width(), 3 * height()/4));
    painter->restore();
}

void TimeLine::drawHandle(QPainter *painter)
{
    painter->save();
    QPen pen(QColor(255, 204, 0));
    painter->setPen(pen);
    pen.setWidth(2);
    QRect textRect = fontMetrics().boundingRect("0000-00-00 00:00:00");
    painter->drawLine(QPointF(width()/2, 0),QPointF(width()/2, height()));
    painter->drawText(QPointF(width()/2 - textRect.width() / 2, textRect.height()), \
                      dateTimeStart.addSecs(-1 * 12.0 * offset.x() / width() * 3600).toString("yyyy-MM-dd hh:mm:ss"));

    painter->restore();
}

void TimeLine::drawTicks(QPainter *painter)
{
    painter->save();
    QPen pen(QColor(0, 255, 0));
    pen.setWidth(1.0);
    painter->setPen(pen);

    qreal step = 1.0 * width() / 12;
    dateTimeStart = dateTimeStart.addSecs(-1 * 12.0 * offset.x() / width() * 3600);

    qreal tickStart = -(dateTimeStart.time().minute() * 60 + dateTimeStart.time().second());  //一直增加
    tickStart = tickStart / 3600 * step;

    QRect textRect = fontMetrics().boundingRect("00:00");

    for(int i = 0; i < 13; i++)
    {
        painter->drawLine(QPointF(tickStart + i * step, 3 * height() / 4),QPointF(tickStart + i * step, height()));
        painter->drawText(QPointF(tickStart + i * step - textRect.width() / 2, 3 * height() / 4 - 1),\
                          dateTimeStart.time().addSecs((i - 6) * 3600).toString("hh:").append("00"));
    }

    textRect = fontMetrics().boundingRect("0000-00-00 00:00:00");
    if(pressDown)
        dateTimeStart = dateTimeStart.addSecs(1 * 12.0 * offset.x() / width() * 3600);
    else
    {
        if(isInside)
            painter->drawText(QPointF(mousePoint.x() - textRect.width() / 2, 3 * height() / 4 - 1 - textRect.height()),\
                          dateTimeStart.addSecs(1.0 * (mousePoint.x() - width() / 2) / step * 3600).toString("yyyy-MM-dd hh:mm:ss"));
    }

    painter->restore();
}
